import sqlite3
import unittest
import threading
from typing import Any

# was using dbm but it kept returning duplicate keys!

class DB:
  def __init__(self, filename: str) -> None:
    self.__mutex = threading.Lock()
    self.con = sqlite3.connect(filename, isolation_level=None, check_same_thread=False)
    self.con.execute("CREATE TABLE IF NOT EXISTS db (key TEXT PRIMARY KEY NOT NULL, value TEXT NOT NULL)")

  def __contains__(self, key: str) -> bool:
    with self.__mutex:
      r = self.con.execute("SELECT 1 FROM db WHERE key = ?", (key, )).fetchone()
      return bool(r)

  def __getitem__(self, key: str) -> str:
    with self.__mutex:
      r = self.con.execute("SELECT value FROM db WHERE key = ?", (key, )).fetchone()
      if not r:
        raise KeyError()
      return str(r[0])

  def get(self, key: str, default: str | None = None) -> str | None:
    try:
      return self[key]
    except KeyError:
      return default

  def keys(self) -> list[str]:
    with self.__mutex:
      return list(str(row[0]) for row in self.con.execute("SELECT key FROM db"))

  def __delitem__(self, key: str) -> None:
    with self.__mutex:
      r = self.con.execute("DELETE FROM db WHERE key = ?", (key, )).rowcount
    if r == 0:
      raise KeyError()

  def __setitem__(self, key: str, value: str) -> None:
    with self.__mutex:
      self.con.execute("INSERT OR REPLACE INTO db (key, value) VALUES (?, ?)", (key, value))

  def close(self) -> None:
    with self.__mutex:
      self.con.close()

  def __enter__(self) -> "DB":
    return self

  def __exit__(self, *exc_details: Any) -> None:
    self.close()

class Test(unittest.TestCase):
  def test(self) -> None:
    with DB(":memory:") as db:
      self.assertFalse("hi" in db)
      self.assertEqual(None, db.get("hi"))
      self.assertEqual("not there", db.get("hi", "not there"))
      with self.assertRaises(KeyError):
        db["hi"]
      self.assertEqual(set(), set(db.keys()))

      db["hi"] = "foo"
      self.assertTrue("hi" in db)
      self.assertEqual("foo", db["hi"])
      self.assertEqual("foo", db.get("hi"))
      self.assertEqual({"hi"}, set(db.keys()))

      db["foo"] = "bar"
      self.assertEqual({"hi", "foo"}, set(db.keys()))

      db["hi"] = "moo"
      self.assertTrue("hi" in db)
      self.assertEqual("moo", db["hi"])
      self.assertEqual("moo", db.get("hi"))
      self.assertEqual({"hi", "foo"}, set(db.keys()))

      del db["hi"]

      self.assertFalse("hi" in db)
      self.assertEqual(None, db.get("hi"))
      self.assertEqual("not there", db.get("hi", "not there"))
      with self.assertRaises(KeyError):
        db["hi"]
      self.assertEqual({"foo"}, set(db.keys()))

      with self.assertRaises(KeyError):
        del db["hi"]
