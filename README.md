# autotranscode

## Purpose

Allows you to keep your flacs in original quality locally easily maintain a second copy for serving up from the cloud

## Features

- mirrors flacs from a source directory, transcoding them into mp3s in a target directory
- automatically keeps target directory in sync with source (deletes, renames, etc.)
- preserves tags, album art

## License

MIT License

## Requirements

- python 3.10
- ffprobe
- ffmpeg

## Usage

See ```./autotranscode -h```
