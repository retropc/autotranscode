import json
import os
import dataclasses
import logging
from dataclasses import dataclass
from typing import Any, Generic, Callable, BinaryIO, Iterable, Type, Self

from util import Json, JsonDict, J

DbType = Any

logger = logging.getLogger(__name__)

@dataclass
class Inode(Json):
  mtime: int
  size: int
  inode: int

  def to_json(self) -> JsonDict:
    j = super().to_json()
    return j | dataclasses.asdict(self)

  @classmethod
  def from_json(cls, j: JsonDict) -> Self:
    cls._validate_json(j)
    return cls(mtime=int(j["mtime"]), inode=int(j["inode"]), size=int(j["size"]))

  @classmethod
  def from_stat(cls, s: os.stat_result) -> Self:
    return cls(mtime=s.st_mtime_ns, size=s.st_size, inode=s.st_ino)

  @classmethod
  def from_fd(cls, fd: int) -> Self:
    return cls.from_stat(os.fstat(fd))

@dataclass
class InodeCacheValue(Generic[J], Json):
  inode: Inode
  value: J | None

  def to_json(self) -> JsonDict:
    j = super().to_json()
    j["inode"] = self.inode.to_json()
    if self.value is not None:
      j["value"] = self.value.to_json()

    return j

  @classmethod
  def from_json_factory(cls, j: JsonDict, factory: Type[J]) -> Self:
    cls._validate_json(j)
    inode = Inode.from_json(j["inode"])
    value_s = j.get("value")

    return cls(inode, value=factory.from_json(value_s) if value_s is not None else None)

  @classmethod
  def from_json(cls, j: JsonDict) -> Self:
    raise Exception("not implemented")

class InodeCache(Generic[J]):
  def __init__(self, cache: DbType, prefix: str, reader: Callable[[BinaryIO], J | None], factory: Type[J]) -> None:
    self.__cache = cache
    self.__prefix = prefix
    self.__reader = reader
    self.__factory = factory

  def __get_key(self, s: str) -> str:
    return self.__prefix + s

  def get_file(self, filename: str, f: BinaryIO) -> J | None:
    inode = Inode.from_fd(f.fileno())

    cached_s = self.__cache.get(self.__get_key(filename))
    if cached_s is not None:
      cached = InodeCacheValue[J].from_json_factory(json.loads(cached_s), factory=self.__factory)
      if cached.inode == inode:
        logger.debug("source metadata for %s looked up from cache", filename)
        return cached.value
      else:
        logger.debug("cache for %s out of date, refetching...", filename)
    else:
      logger.debug("source metadata for %s not cached, looking up...", filename)

    value = self.__reader(f)
    self.__set(filename, InodeCacheValue(inode=inode, value=value))
    return value

  def get(self, filename: str) -> J | None:
    with open(filename, "rb") as f:
      return self.get_file(filename, f)

  def __set(self, filename: str, cv: InodeCacheValue[J]) -> None:
    self.__cache[self.__get_key(filename)] = json.dumps(cv.to_json())

  def gc(self, referents: Iterable[str]) -> None:
    logger.debug("starting cache GC...")
    referents_s = set(self.__get_key(x) for x in referents)
    for x in self.__cache.keys():
      if not x.startswith(self.__prefix):
        continue
      if x not in referents_s:
        logger.debug("GC'ing %s", x)
        del self.__cache[x]
    logger.debug("cache GC complete")
