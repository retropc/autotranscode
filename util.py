from abc import ABC, abstractmethod
from typing import Any, TypeVar, Self

JsonDict = dict[str, Any]

J = TypeVar("J", bound="Json")

class Json:
  def to_json(self) -> JsonDict:
    return {"__js": self.__class__.__name__}

  @classmethod
  def _validate_json(cls, j: JsonDict) -> None:
    if j["__js"] != cls.__name__:
      raise Exception(f"bad serialised type: {j!r}")

  @classmethod
  @abstractmethod
  def from_json(cls, j: JsonDict) -> Self:
    pass
